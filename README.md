# Universal eMMC dumper for android recovery

Simple update.zip shell script that exposes block device (like eMMC)
as USB Mass Storage device

## Possible configuration

To change block device exposed by default and RW/RO, change `device` and
`device_ro` variables in `META-INF/com/google/android/update-binary`.
When using sideload only default values will be used!

When launching from "Install Zip" (and NOT from sideload) filename can be used
to override used block device and RW/RO. For example `mmcblk1-rw.zip`
will expose `/dev/block/mmcblk1` as writable pendrive,
but if device doesn't exist it will fallback to defaults set in `update-binary`

`update-binary` is actually not a binary, but a shell script executed by `/sbin/sh`

## TODO
* sign the update.zip with something that is more portable
(compilation included) than [SignApk.java]
(https://android.googlesource.com/platform/build/+/android-8.1.0_r41/tools/signapk/src/com/android/signapk/SignApk.java)

## License

This project is licensed under the GPLv2 License - see
the [COPYING](COPYING) file for details
